import LocationeModel from '../../../model/location.model'
// Interface for Vehicle List View Model
export interface  LocationListViewModelInterface{
    location:LocationeModel[];
    getLocation():LocationeModel[]

}
