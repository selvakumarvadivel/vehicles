import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle Search View Model
export default interface  VehicleSearchViewModelInterface{
    location:string[];
    vehicleType:string[];
    getLocation():string[];
    getVehicleType():string[];
    isValidDate(date:string):boolean
    isValidPickingUpDate(startDate:Date,endDate:Date):boolean;
    isValiddroppingOffDate(startDate:Date,endDate:Date):boolean;
}

