import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle Search View Model
export interface  VehicleSearchViewInterface{
    // Routing to Vehicle List
    searchVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):void;

}

