import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle List View Model
export interface  VehicleListViewControllerInterface{
    vehicle : VehicleModel[] ;
    getVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];
    placeOrder(vehicleId:Number,pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):string
    showVehicleDetails(vehicleId:Number):void;
}
