import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle List View Model
export interface  VehicleListViewModelInterface{
    vehicle : VehicleModel[] ;
    getVehicle(pickingUplocation:string,droppingOffLocation:string,vehicleModel:string,pickingUpDate:string,droppingOffDate:string):VehicleModel[];

}
