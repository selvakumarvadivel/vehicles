import VehicleModel from '../../../model/vehicle.model'
// Interface for Vehicle List View Model
export interface  VehicleListViewInterface{
    vehicle : VehicleModel[] ;
    showVehicleDetails():void;
}
